#-----------------------------------------------------------------
#  Shotgun vs rescap - shotgun part
#-----------------------------------------------------------------
table <- matrix(c("empty"),ncol=3,byrow=TRUE)
colnames(table) <- c("Diet","median", "SD")

ttest_shotgun <- matrix(c("empty"),ncol=3,byrow=TRUE)
colnames(ttest_shotgun) <- c("Diet","ID", "ARGcount")


shot_res_samples = { c("S1","S3","S4","S5","S10","S13","S17",
                       "S19","S20","S22","S24","S26","S27",
                       "S29","S30","S31","S32","S33","S36",
                       "S37","S38","S39","S40","S41","S42",
                       "S43","S45","S47","S48","S49","S50",
                       "S58","S59","S60","S61","S64",
                       "S67","S68","S69","S74","S76","S77",
                       "S80","S87","S88","S92","S93","S100",
                       "S101","S109","S112","S118","S128","S129",
                       "S131","S133","S136","S139","S140","S141",
                       "S142","S143","S146","S148")} # select samples that are sequenced with ResCap
shotres_rare<-phyl_shotres
shotres_rare
shotres_rare <- subset_samples(shotres_rare, ID  %in% shot_res_samples)

for (group in unique(as.character(meta(shotres_rare)$Diet))){
  plot_data <- shotres_rare
  plot_data <- subset_samples(plot_data, Diet %in% c(group))
  otu_mat <- as(otu_table(plot_data), "matrix")
  otu_mat[otu_mat > 0] <- 1 #replace positive count for 1
  count_data <- data.frame(ID=colnames(otu_mat), ARGcount=colSums(otu_mat))
  sum_data <- data.frame(Diet = c(group),
                         median=median(count_data$ARGcount),
                         SD = sd(count_data$ARGcount)
  )
  table <- rbind(table, sum_data)
  count_data$Diet <- group
  ttest_shotgun <- rbind(ttest_shotgun, count_data)
  
  rm(plot_data)|rm(otu_mat)|rm(count_data)
}

table <- table[-c(1),]
table[,-1] <- data.frame(apply(table[,-1],
                               2, function(x) as.numeric(as.character(x))))#to numeric
table_shotgun <- table
table_shotgun$technique <- "Shotgun"

rm(table)

ttest_shotgun <- ttest_shotgun[-c(1),]
ttest_shotgun$ARGcount <- as.numeric(ttest_shotgun$ARGcount)#to numeric

#-----------------------------------------------------------------
#  shotgun vs rescap - rescap part
#-----------------------------------------------------------------
table <- matrix(c("empty"),ncol=3,byrow=TRUE)
colnames(table) <- c("Diet","median", "SD")

ttest_rescap <- matrix(c("empty"),ncol=3,byrow=TRUE)
colnames(ttest_rescap) <- c("Diet","ID", "ARGcount")

for (group in unique(as.character(meta(phyl_rescap)$Diet))){
  plot_data <- phyl_rescap
  plot_data <- subset_samples(plot_data, Diet %in% c(group))
  otu_mat <- as(otu_table(plot_data), "matrix")
  otu_mat[otu_mat > 0] <- 1 #replace positive count for 1
  count_data <- data.frame(ID=colnames(otu_mat), ARGcount=colSums(otu_mat))
  sum_data <- data.frame(Diet = c(group),
                           median=median(count_data$ARGcount),
                           SD = sd(count_data$ARGcount)
  )
  table <- rbind(table, sum_data)
  count_data$Diet <- group
  ttest_rescap <- rbind(ttest_rescap, count_data)
  
  rm(plot_data)|rm(otu_mat)|rm(count_data)
}

table <- table[-c(1),]
table[,-1] <- data.frame(apply(table[,-1],
                               2, function(x) as.numeric(as.character(x))))#to numeric
table_rescap <- table
table_rescap$technique <- "ResCap"
rm(table)

ttest_rescap <- ttest_rescap[-c(1),]
ttest_rescap$ARGcount <- as.numeric(ttest_rescap$ARGcount)#to numeric

#merge and shape table
table_rare <- rbind(table_rescap, table_shotgun)
table_rare$technique <- factor(table_rare$technique)
rm(table_rescap)|rm(table_shotgun)

table_rare$Diet <- factor(table_rare$Diet, levels =
                                         c("omnivore", "pescatarian",
                                           "vegetarian", "vegan"))

#add PAIRED t-test
colnames(ttest_rescap)[colnames(ttest_rescap) == 'ARGcount'] <- 'rescap'
colnames(ttest_shotgun)[colnames(ttest_shotgun) == 'ARGcount'] <- 'shotgun'
ttest <- plyr::join(ttest_rescap,ttest_shotgun, by="ID", type = "full") #merge
table <- matrix(c("empty"),ncol=2,byrow=TRUE)
colnames(table) <- c("Diet","pval")
for (group in unique(as.character(ttest$Diet))){
  plot_data <- dplyr::filter(ttest, grepl(group,Diet))
  count_data <- t.test(ttest$rescap, ttest$shotgun, paired = TRUE,
                       alternative = "two.sided") 
  table <- rbind(table, data.frame(Diet = group,
                                   pval=count_data$`p.value`))
  rm(group)|rm(plot_data)|rm(count_data)
}
table <- table[-c(1),]
table$pval <- stars.pval(as.numeric(table$pval))
table$group1 <- "ResCap"
table$group2 <- "Shotgun"
table$y.position <- (52)
                        
#plot
ggplot(table_rare)+
  facet_grid(.~factor(Diet, levels=c("omnivore","pescatarian","vegetarian","vegan")))+
  geom_bar(aes(x=technique, y=median, fill = Diet), stat="identity")+
  geom_errorbar(aes(x = technique, ymin=median-SD, ymax=median+SD), 
                 width=0.4, colour="grey20", alpha=0.9, size=1) +
  scale_fill_manual("Diet", values = cols_diet)+
  theme_bw() +
  ylab( "Detected ARGs") +
  
  stat_pvalue_manual(table, label = "pval", tip.length = 0.01,
                     bracket.nudge.y = -0, bracket.shorten = 0.2) +
  
  theme(axis.title.x = element_blank(),
        legend.position = "none",
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        axis.text.x =element_text(color="Grey1", size=11, angle = 90))


ggsave(paste(location_plots,sep="", "/rescap_shotgun_counts_median.tiff"),
       units="in", width=4, height=5, dpi=300, compression = 'lzw')

rm(ttest_rescap)|rm(ttest_shotgun)

#-----------------------------------------------------------------
#  Shotgun only plot
#-----------------------------------------------------------------
table <- matrix(c("empty"),ncol=3,byrow=TRUE)
colnames(table) <- c("Diet","median", "SD")

ttest_rescap <- matrix(c("empty"),ncol=3,byrow=TRUE)
colnames(ttest_rescap) <- c("Diet","ID", "ARGcount")

for (group in unique(as.character(meta(phyl_shotres)$Diet))){
  plot_data <- phyl_shotres
  plot_data <- subset_samples(plot_data, Diet %in% c(group))
  otu_mat <- as(otu_table(plot_data), "matrix")
  otu_mat[otu_mat > 0] <- 1 #replace positive count for 1
  count_data <- data.frame(ID=colnames(otu_mat), ARGcount=colSums(otu_mat))
  sum_data <- data.frame(Diet = c(group),
                         median=median(count_data$ARGcount),
                         SD = sd(count_data$ARGcount)
  )
  table <- rbind(table, sum_data)
  count_data$Diet <- group
  ttest_rescap <- rbind(ttest_rescap, count_data)
  
  rm(plot_data)|rm(otu_mat)|rm(count_data)
}

table <- table[-c(1),]
table[,-1] <- data.frame(apply(table[,-1],
                               2, function(x) as.numeric(as.character(x))))#to numeric
table_shotgun <- table
table_shotgun$technique <- "Shotgun"
rm(table)

#shape table
table_shotgun$Diet <- factor(table_shotgun$Diet, levels =
                            c("omnivore", "pescatarian",
                              "vegetarian", "vegan"))

#plot
ggplot(table_shotgun)+
  facet_grid(.~Diet)+
  geom_bar(aes(x=technique, y=median, fill = Diet), stat="identity")+
  geom_errorbar(aes(x = technique, ymin=median-SD, ymax=median+SD), 
                width=0.4, colour="grey20", alpha=0.9, size=1) +
  scale_fill_manual("Diet", values = cols_diet)+
  theme_bw() +
  ylab( "Detected ARGs") +
  
  theme(axis.title.x = element_blank(),
        legend.position = "none",
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        axis.text.x =element_blank())


ggsave(paste(location_plots,sep="", "/shotgun_counts_median.tiff"),
       units="in",width=4, height=3, dpi=300, compression = 'lzw')

rm(ttest_rescap)|rm(ttest_shotgun)
