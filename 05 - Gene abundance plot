#-----------------------------------------------------------------
#  Resistome by shotgun metagenomic sequencing
#-----------------------------------------------------------------
pseq_heat <- phyl_shotres

#Remove genes that appear less than 5 times in 10% of the samples
pseq_heat <- core(pseq_heat, detection = 5, prevalence = 15/nsamples(pseq_heat))
pseq_heat <- pseq.long(pseq_heat, transform.counts = "compositional")
pseq_heat <- pseq_heat[order(pseq_heat$abundance,decreasing = F),]

#Split data into two groups
unique(pseq_heat$Class)
pseq_heat_a <- subset(pseq_heat, Class %in% c("tetracycline", "macrolide","beta-lactam"))
pseq_heat_b <- subset(pseq_heat, Class %in% c("aminoglycoside", "phenicol", "sulphonamide"))

plot_a <- ggplot(pseq_heat_a, aes(x = sample, y = reorder(Order,abundance)))+
  geom_tile(aes(fill = log10(abundance)), color = "white")+
  facet_grid(cols= vars(Diet), rows=vars(Class), 
             scales = "free", space="free") +
  scale_fill_distiller(palette = "Spectral", na.value = "white") +
  theme_bw() +
  theme(axis.text.x = element_blank() ,
        axis.ticks.x=element_blank(),
        strip.text.y.right = element_text(angle=0)
  )

plot_b <- ggplot(pseq_heat_b, aes(x = sample, y = reorder(Order,abundance)))+
  geom_tile(aes(fill = log10(abundance)), color = "white")+
  facet_grid(cols= vars(Diet), rows=vars(Class), 
             scales = "free", space="free") +
  scale_fill_distiller(palette = "Spectral", na.value = "white") +
  theme_bw() +
  rotate_x_text() +
  theme(axis.text.x = element_blank() ,
        axis.ticks.x=element_blank(),
        strip.text.y.right = element_text(angle=0)
  )

#plot seperate
shared_plot = ggarrange(plot_a+ylab(" ")+xlab(" ")+theme(strip.background=element_blank()),
                        plot_b+ylab(" ")+xlab(" ")+theme(strip.background=element_blank()),
                        ncol = 1, nrow = 2,
                        heights = c(5, 2),
                        align = "v",
                        common.legend = T, legend = "bottom")
shared_plot

ggsave(paste(location_plots,sep="", "/Heat_shotres.tiff"),
       units="in", width=8, height=6, dpi=300, compression = 'lzw')

rm(pseq_heat)|rm(plot_a)|rm(plot_b)|rm(pseq_heat_a)|rm(pseq_heat_b)|rm(shared_plot)

#-----------------------------------------------------------------
#  Resistome by ResCap targeted sequencing
#-----------------------------------------------------------------

pseq_heat <- phyl_rescap
#Remove OTUs that appear less than 5 times in 10% of the samples
pseq_heat <- core(pseq_heat, detection = 5, prevalence = 6/nsamples(pseq_heat))
pseq_heat <- pseq.long(pseq_heat, transform.counts = "compositional")
pseq_heat <- pseq_heat[order(pseq_heat$abundance,decreasing = F),]

#Split data into two groups
unique(pseq_heat$Class)
pseq_heat_a <- subset(pseq_heat, Class %in% c("tetracycline", "macrolide","beta-lactam"))
pseq_heat_b <- subset(pseq_heat, Class %in% c("aminoglycoside", "glycopeptide","phenicol", "sulphonamide"))

plot_a <- ggplot(pseq_heat_a, aes(x = sample, y = reorder(Order,abundance)))+
  geom_tile(aes(fill = log10(abundance)), color = "white")+
  facet_grid(cols= vars(Diet), rows=vars(Class), 
             scales = "free", space="free") +
  scale_fill_distiller(palette = "Spectral", na.value = "white") +
  theme_bw() +
  theme(axis.text.x = element_blank() ,
        axis.ticks.x=element_blank(),
        strip.text.y.right = element_text(angle=0)
  )

plot_b <- ggplot(pseq_heat_b, aes(x = sample, y = reorder(Order,abundance)))+
  geom_tile(aes(fill = log10(abundance)), color = "white")+
  facet_grid(cols= vars(Diet), rows=vars(Class), 
             scales = "free", space="free") +
  scale_fill_distiller(palette = "Spectral", na.value = "white") +
  theme_bw() +
  rotate_x_text() +
  theme(axis.text.x = element_blank() ,
        axis.ticks.x=element_blank(),
        strip.text.y.right = element_text(angle=0)
  )

#plot seperate
shared_plot = ggarrange(plot_a+ylab(" ")+xlab(" ")+theme(strip.background=element_blank()),
                        plot_b+ylab(" ")+xlab(" ")+theme(strip.background=element_blank()),
                        ncol = 1, nrow = 2,
                        heights = c(20, 9),
                        align = "v",
                        common.legend = T, legend = "bottom")
shared_plot

ggsave(paste(location_plots,sep="", "/Heat_rescap.tiff"),
       units="in", width=8, height=8, dpi=300, compression = 'lzw')

rm(pseq_heat)|rm(plot_a)|rm(plot_b)|rm(pseq_heat_a)|rm(pseq_heat_b)|rm(shared_plot)
