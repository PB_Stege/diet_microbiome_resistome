#-----------------------------------------------------------------
#  Microbiome
#-----------------------------------------------------------------
#Plot Genus grouped by diet
physeq_class <- microbiome::aggregate_top_taxa(phyl_motu, "Genus", top = 10) #reduce to class
physeq_class <- microbiome::transform(physeq_class, "compositional") #compositional
PaletteChoice = rev(rainbow(length(taxa_sums(physeq_class))))   #pick colors
PaletteChoice = gsub("FF008B", "DCDCDC", PaletteChoice) #add grey
PaletteChoice = gsub("00FF2E", "3CB371", PaletteChoice) #replace shade of green

#adjust names
otu_mat = as(otu_table(physeq_class), "matrix")
taxa_names(physeq_class) <- gsub('g__', "",taxa_names(physeq_class), 
                                 ignore.case = F)

taxa_names(physeq_class) <- gsub('Ruminococcaceae.*', "fam. Ruminococcaceae",taxa_names(physeq_class), 
                                 ignore.case = F)

taxa_names(physeq_class) <- gsub('Clostridiales.*', "ord. Clostridiales",taxa_names(physeq_class), 
                                 ignore.case = F)

{plot.composition.relAbun <- plot_composition(physeq_class, 
                                              sample.sort = NULL,
                                              otu.sort = c("Other",
                                                           names(sort(taxa_sums(
                                                               physeq_class),F)[1:10])
                                                           ), #adjust order "Other"
                                              x.label = "Sample",
                                              average_by = "Diet")
  #add settings plot
  plot.composition.relAbun + 
    scale_fill_manual(values=c(PaletteChoice)) +
    theme_bw() +
      
    theme(legend.justification  = "top",
          axis.title.x = element_blank(),
          legend.title = element_blank(),
          plot.title = element_text(size = 13, face = "bold"),
          axis.title.y = element_text(color="Grey1", size=11),
          axis.text.x =element_text(color="Grey1", size=11, angle = 90),
          axis.text=element_text(color="Grey1", size=11),
          panel.grid.major = element_blank(),
          axis.ticks = element_blank(),
          legend.text = element_text(size = 10, face = "italic")
    )
  #+scale_fill_brewer("Class", palette = "Paired") change color palette
}
ggsave(paste(location_plots,sep="", "/RelA_shotmotu.tiff")
             , units="in", width=5, height=5, dpi=300, compression = 'lzw')

rm(tax_mat)|rm(physeq_class)|rm(plot.composition.relAbun)

#-----------------------------------------------------------------
#  Resistome by shotgun metagenomic sequencing
#-----------------------------------------------------------------
#Plot class grouped by diet
physeq_class <- microbiome::aggregate_top_taxa(phyl_shotres, "Class", top = 10) #reduce to class
physeq_class <- microbiome::transform(physeq_class, "compositional") #compositional
tax_mat = as(sample_data(phyl_shotres), "matrix") # shape taxonomy table
PaletteChoice = rev(rainbow(length(taxa_sums(physeq_class))))   #pick colors
PaletteChoice = gsub("FF008B", "DCDCDC", PaletteChoice) #add grey
PaletteChoice = gsub("00FF2E", "3CB371", PaletteChoice) #replace shade of green

{plot.composition.relAbun <- plot_composition(physeq_class, 
                                              sample.sort = NULL,
                                              otu.sort = names(sort(taxa_sums(physeq_class), F)[1:11]),
                                              x.label = "Sample",
                                              average_by = "Diet")
  #add settings plot
  plot.composition.relAbun + 
    scale_fill_manual(values=c(PaletteChoice)) + 
    theme_bw() +
      
    theme(legend.justification  = "top",
          axis.title.x = element_blank(),
          legend.title = element_blank(),
          plot.title = element_text(size = 13, face = "bold"),
          axis.title.y = element_text(color="Grey1", size=11),
          axis.text.x =element_text(color="Grey1", size=11, angle = 90),
          axis.text=element_text(color="Grey1", size=11),
          panel.grid.major = element_blank(),
          axis.ticks = element_blank()
    )
  #+scale_fill_brewer("Class", palette = "Paired") change color palette
}
ggsave(paste(location_plots,sep="", "/RelA_ShotRes.tiff"),
       units="in", width=5, height=5, dpi=300, compression = 'lzw')

rm(plot.composition.relAbun)|rm(tax_mat)|rm(physeq_class)

#-----------------------------------------------------------------
#  Resistome by ResCap targeted sequencing
#-----------------------------------------------------------------
#Plot class grouped by diet
physeq_class <- microbiome::aggregate_top_taxa(phyl_rescap, "Class", top = 10) #reduce to class
physeq_class <- microbiome::transform(physeq_class, "compositional") #compositional
tax_mat = as(sample_data(phyl_shotres), "matrix") # shape taxonomy table
PaletteChoice = rev(rainbow(length(taxa_sums(physeq_class))))   #pick colors
PaletteChoice = gsub("00FF2E", "3CB371", PaletteChoice) #replace shade of green

{plot.composition.relAbun <- plot_composition(physeq_class, 
                                              sample.sort = NULL,
                                              otu.sort = names(sort(taxa_sums(physeq_class), F)[1:11]),
                                              x.label = "Sample",
                                              average_by = "Diet")
  #add settings plot
  plot.composition.relAbun + 
    scale_fill_manual(values=c(PaletteChoice)) + 
    theme_bw() +
      
    theme(legend.justification  = "top",
          axis.title.x = element_blank(),
          legend.title = element_blank(),
          plot.title = element_text(size = 13, face = "bold"),
          axis.title.y = element_text(color="Grey1", size=11),
          axis.text.x =element_text(color="Grey1", size=11, angle = 90),
          axis.text=element_text(color="Grey1", size=11),
          panel.grid.major = element_blank(),
          axis.ticks = element_blank()
    )
  #+scale_fill_brewer("Class", palette = "Paired") change color palette
}
ggsave(paste(location_plots,sep="", "/RelA_ResCap.tiff"),
       units="in", width=4, height=4, dpi=300, compression = 'lzw')

rm(plot.composition.relAbun)|rm(tax_mat)|rm(physeq_class)
