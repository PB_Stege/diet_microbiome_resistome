**Study title: Limited impact of long-term dietary habits on the human gut microbiome and resistome in the Dutch population**

Authors: Paul B. Stege, Joost Hordijk, Sudarshan A. Shetty, Michael Visser,3, Marco C. Viveen, Malbert Rogers, Esther Gijsbers, Cindy M. Dierikx, Rozemarijn Q.J. van der Plaats, Engeline van Duijkeren, Eelco Franz, Rob J. L. Willems, Susana Fuentes, Fernanda L. Paganelli.

This repository contains the code used for analysis.




Applied R packages include

_vegan_2.5-6 Oksanen, Jari, et al. “The vegan package.” Community ecology package 10.631-637 (2007): 719_

_phyloseq_1.30.0 McMurdie, Paul J., and Susan Holmes. “phyloseq: an R package for reproducible interactive analysis and graphics of microbiome census data.” PloS one 8.4 (2013): e61217_

_microbiome_2.1.24 Lahti, Leo, and Sudarshan Shetty. “Introduction to the microbiome R package.”_

_microbiomeutilities_0.99.02 Sudarshan A. Shetty, & Leo Lahti. (2018, October 25). microbiomeutilities: An R package for utilities to guide in-depth marker gene amplicon data analysis 10. doi 5281/zenodo.1471685. Zenodo_

